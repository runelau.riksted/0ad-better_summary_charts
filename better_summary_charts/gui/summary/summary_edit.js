let g_OldGameData = { "sim":{ "playerStates":[] }};
let g_LoadedOldGameData = false;


function LoadOldGameData(){

	//global.warn("Load OldGameData")
	
	let playername = {"multiplayer":Engine.ConfigDB_GetValue("user", "playername.multiplayer"),
		"singleplayer":Engine.ConfigDB_GetValue("user", "playername.singleplayer")}
	
	let replayList = Engine.GetReplays().sort(function(x,y){return x.fileMTime - y.fileMTime;});
	let acptedReplays = 0;
	let replayDirectory = " ";
	
	if (Engine.IsVisualReplay())
	{
		replayDirectory = Engine.GetCurrentReplayDirectory();
	}else if(g_GameData.gui.replayDirectory)
	{
		replayDirectory = g_GameData.gui.replayDirectory ;
	}else{
		//It is a live game
		replayDirectory = null;
	}
	
	for ( let index in replayList)
	{
		if (replayList[index].directory == replayDirectory
			|| g_GameData.sim.mapSettings.StartingResources != replayList[index].attribs.settings.StartingResources)
		{
			continue;
		}
		
		let players = replayList[index].attribs.settings.PlayerData;
		
		for (let i in players){
			
			if( players[i].Name.includes(playername.singleplayer) || players[i].Name.includes(playername.multiplayer) )
			{	
				
				let replay_meta = Engine.GetReplayMetadata(replayList[index].directory);
				
				if (replay_meta)
				{
					
					i++; // Add 1 to compensate for Gaia being idnex 0 in Engine.GetReplayMetadata()
					
					g_OldGameData.sim.playerStates.push( replay_meta.playerStates[i] );
					
					acptedReplays ++;
					break;
				}
			}
			
		}
		if (acptedReplays >= 10){
			break;
		} 	
	}
	
	g_LoadedOldGameData = true;
}

function updateChartColorAndLegend()
{	
	//global.warn("updateChartColorAndLegend (" + (g_LoadedOldGameData) + ")")
	if((!g_LoadedOldGameData) && Engine.GetGUIObjectByName("togglePreviousGamesBox").checked){
		LoadOldGameData();
	}
	
	let playerColors = [];
	let playername = {"multiplayer":Engine.ConfigDB_GetValue("user", "playername.multiplayer"),
		"singleplayer":Engine.ConfigDB_GetValue("user", "playername.singleplayer")}
	let userColorFaint = "0 0 0 100";
	for (let i = 1; i <= g_PlayerCount; ++i)
	{
		let playerState = g_GameData.sim.playerStates[i];
		if( playerState.name.includes(playername.singleplayer)
			|| playerState.name.includes(playername.multiplayer) )
		{
			userColorFaint =
				Math.floor(playerState.color.r * 255) + " " +
				Math.floor(playerState.color.g * 255) + " " +
				Math.floor(playerState.color.b * 255) + " 100";
		}
		
		playerColors.push(
			Math.floor(playerState.color.r * 255) + " " +
			Math.floor(playerState.color.g * 255) + " " +
			Math.floor(playerState.color.b * 255) + " 255"
		);
	}
	playerColors = playerColors.concat( Array(g_OldGameData.sim.playerStates.length).fill(userColorFaint) );
	
	for (let i = 0; i < 2; ++i)
		Engine.GetGUIObjectByName("chart[" + i + "]").series_color =
			Engine.GetGUIObjectByName("toggleTeamBox").checked ?
				g_Teams.filter(el => el !== null).map(players => playerColors[players[0] - 1]) :
				playerColors;

	let chartLegend = Engine.GetGUIObjectByName("chartLegend");
	let playerStates =  Array.from(g_GameData.sim.playerStates);
	
	if (Engine.GetGUIObjectByName("togglePreviousGamesBox").checked && g_OldGameData.sim.playerStates.len > 0){
		playerStates.push({"name":"Previous Games"});
	}
	
	
	chartLegend.caption = (Engine.GetGUIObjectByName("toggleTeamBox").checked ?
		g_Teams.filter(el => el !== null).map(players =>
			constructPlayersWithColor(playerColors[players[0] - 1],	players.map(player =>
				playerStates[player].name
			).join(translateWithContext("Player listing", ", ")))
		) :
		playerStates.slice(1).map((state, index) =>
			constructPlayersWithColor(playerColors[index], state.name))
	).join("   ");
	
}


function updateChart(number, category, item, itemNumber, type)
{
	//global.warn("updateChart");
	
	
	if (!g_ScorePanelsData[category].counters[itemNumber].fn)
		return;
	let chart = Engine.GetGUIObjectByName("chart[" + number + "]");
	chart.format_y = g_ScorePanelsData[category].headings[itemNumber + 1].format || "INTEGER";
	Engine.GetGUIObjectByName("chart[" + number + "]XAxisLabel").caption = translate("Time elapsed");

	let series = [];
	let sequenceLength = g_GameData.sim.playerStates[1].sequences.time.length;
	let lastTime = g_GameData.sim.playerStates[1].sequences.time[sequenceLength-1];
	
	
	function plot_player(playerState)
	{
		let data = [];
		let index_max = Math.min(sequenceLength, playerState.sequences.time.length)
		for (let index = 0; index <index_max; ++index)
		{
			let value = g_ScorePanelsData[category].counters[itemNumber].fn(playerState, index, item);
			if (type)
				value = value[type];
			data.push({ "x": playerState.sequences.time[index], "y": value });
		}
		
		if (lastTime < playerState.sequences.time[sequenceLength-1])
		{
			// If the current preview ended on an odd time, the historical previews' point at that index will be up to 30s later, thus we interpolate.
			let dt = lastTime - playerState.sequences.time[sequenceLength-2];
			let y1 = data[sequenceLength-2].y;
			let y2 = data[sequenceLength-1].y;
			let dT = playerState.sequences.time[sequenceLength-1]-playerState.sequences.time[sequenceLength-2];
			
			data[sequenceLength-1] = { "x": lastTime, "y": y1+dt*(y2-y1)/dT };
		}
		
		series.push(data);
	
	}
	
	
	if (Engine.GetGUIObjectByName("toggleTeamBox").checked)
		for (let team in g_Teams)
		{
			let data = [];
			for (let index in g_GameData.sim.playerStates[1].sequences.time)
			{
				let value = g_ScorePanelsData[category].teamCounterFn(team, index, item,
					g_ScorePanelsData[category].counters, g_ScorePanelsData[category].headings);
				if (type)
					value = value[type];
				data.push({ "x": g_GameData.sim.playerStates[1].sequences.time[index], "y": value });
			}
			series.push(data);
		}
	else
	{
		for (let j = 1; j <= g_PlayerCount; ++j)
		{
			plot_player(g_GameData.sim.playerStates[j]);
		}
		if((g_LoadedOldGameData) && Engine.GetGUIObjectByName("togglePreviousGamesBox").checked){
			for (let j in g_OldGameData.sim.playerStates)
			{
				plot_player(g_OldGameData.sim.playerStates[j]);
			}
		}
	}
	
	chart.series = series;


	let size = chart.getComputedSize();
	let pixel2index = g_GameData.sim.timeElapsed/1000
			/g_GameData.sim.playerStates[1].sequences.time[1]
			/(size.right-size.left);
		
	// Add and update a tooltip to the Chart, showing the y-values for each player.
	chart.onMouseMove = function(mouse){
	
		// Calculate the index colsest to the time the mouse use is hoverieng over.
		let index = ( pixel2index*(mouse.x - size.left) ).toFixed(0);
		
		// Get and sort the [player-color, value] pairs
		let values = [];
		
		for (let series in chart.series)
		{
			if (index < chart.series[series].length)
			{
				values.push([setStringTags("■ ", { "color": chart.series_color[series] }),
					chart.series[series][index].y]
				);
			}
		}
		
		values.sort(function (x,y){return y[1]-x[1]});
		
		// Format the numbers
		switch (chart.format_y)
		{
		case "PERCENTAGE":
			values = values.map(value => value[0] + value[1] + "%");
			break;
			
		case "DECIMAL2":
			values = values.map(value => value[0] + (Infinity === value[1] ?
				"  " + g_InfinitySymbol : value[1].toFixed(2)
			));
			break;
			
		default:
			// Integers and default:
			values = values.map(value => value[0] + value[1]);
		}
		
		chart.tooltip = timeToString(1000*g_GameData.sim.playerStates[1].sequences.time[index])
			+ ":\n" + values.join("\n");
	};
}
/**
 * Selected chart indexes.
 */
g_TabCategorySelected = 6;
g_SelectedChart["category"][1] = 5;
g_SelectedChart["value"][1] = 1;


function initSummaryData(data)
{
	g_GameData = data;
	g_ScorePanelsData = getScorePanelsData();

	let teamCharts = false;
	let previousGames = false;
	if (data && data.gui && data.gui.summarySelection)
	{
		g_TabCategorySelected = data.gui.summarySelection.panel;
		g_SelectedChart = data.gui.summarySelection.charts;
		teamCharts = data.gui.summarySelection.teamCharts;
		previousGames = data.gui.summarySelection.previousGames
	}
	Engine.GetGUIObjectByName("toggleTeamBox").checked = g_Teams && teamCharts;
	Engine.GetGUIObjectByName("togglePreviousGamesBox").checked = previousGames;

	initTeamData();
	calculateTeamCounterDataHelper();
}


function continueButton()
{
	let summarySelection = {
		"panel": g_TabCategorySelected,
		"charts": g_SelectedChart,
		"teamCharts": Engine.GetGUIObjectByName("toggleTeamBox").checked,
		"previousGames":Engine.GetGUIObjectByName("togglePreviousGamesBox").checked
	};
	if (g_GameData.gui.isInGame)
		Engine.PopGuiPage({
			"summarySelection": summarySelection
		});
	else if (g_GameData.gui.dialog)
		Engine.PopGuiPage();
	else if (Engine.HasXmppClient())
		Engine.SwitchGuiPage("page_lobby.xml", { "dialog": false });
	else if (g_GameData.gui.isReplay)
		Engine.SwitchGuiPage("page_replaymenu.xml", {
			"replaySelectionData": g_GameData.gui.replaySelectionData,
			"summarySelection": summarySelection
		});
	else if (g_GameData.campaignData)
		Engine.SwitchGuiPage(g_GameData.nextPage, g_GameData.campaignData);
	else
		Engine.SwitchGuiPage("page_pregame.xml");
}
