# Better summary charts for 0 A.D.
This is a mod for the game "0 A.D." by Wildfire Games (https://play0ad.com/).


## Why this mod?
This mod is for all the 0 A.D. players who prefer the "Strategy" part of "Real Time Strategy". When I finish a game I want to see how I did and how I compare to the other players. The charts/graphs displayed in the end of game summary are perfect for this, but I find it very hard to read values off the graphs, making it hard to make comparisons and I have to switch back and fourth between replays to see if I did better or worse this time. This mod makes it easy to get the exact values for any point in time during the game and it makes it easy to compare to how you did in previous games. 


## Added Features
1. A tooltip is added to the charts found on the summary screen, displayed at the end of a game. When the mouse hovers over a chart a tooltip is shown, displaying the y-values of each player's graph, at the (approximate) point in time the mouse is hovering over. 
1. An option to show your previous games is added, if checked (And "Group by team" is off) your graphs from your 10 most recent games will also be plotted, only games where you stated with the same amount of resources are plotted.


Here is an example of how the tooltip looks:
    
![alt text](example_picture.png "Title Text")




## Installation guide
To install the mod simply do as follows (Works on Linux).

1. Download the better_summary_charts.pyromod file
1. Open a terminal in the directory containing the file you downloaded and run the command:
	"pyrogenesis better_summary_charts.pyromod"
1. The game should now have opened the "mod selection"/"Modifications" screen. Select "better_summary_charts" in the top window, click "Enable" bottom left and then "Save and restart bottom right.

To test the mod go to the Replay menu. Select a game and press "Summary", select "Charts" and hold the mouse still over the Chart for a second.

For more information about installing mods see "https://trac.wildfiregames.com/wiki/Modding_Guide#Howtoinstallmods".


### Multiplayer compatibility
This mod declares it self multiplayer compatible, thus you should be able to use it along side the standard game and most other mods without problems.
